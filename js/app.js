const showWatch = () => {
  let date_1 = new Date();
  let hr = formato(date_1.getHours());
  let min = formato(date_1.getMinutes());
  let sec = formato(date_1.getSeconds());
  let text = " ";
  text = `${hr}: ${min}: ${sec}`;
  document.getElementById("watch").innerHTML = text;
  const month = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fry", "Sat"];
  let weekday = days[date_1.getDay()];
  let day = date_1.getDate();
  let month__2 = month[date_1.getMonth()];

  document.getElementById("date").innerHTML = ` ${weekday}, ${month__2} ${day}`;
  document.getElementById("container").classList.toggle("animar");
};

const formato = (value) => {
  if (value < 10) value = "0" + value;
  return value;
};

document.getElementById("body").onload = showWatch;

setInterval(showWatch, 1000);
